<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::group(['domain' => '{account}.localhost'], function () {
    Route::get('/', function ($account) {
        return $account;
    });
});

Route::get('/{all}', function() {
    return View::make('index');
})->where('all', '[0-9A-Za-z\/-]+');


Route::get('/', function () {
    return view('index');
});

