import React, { Component } from "react";
import HyperLink from "../styled/HyperLink";
class MasterHeader extends Component {
    render() {
        return (
            <div className="row">
                <div className="col d-flex">
                    <div className="borders-right">
                        <a className="navbar-brand nav-link" href="#">
                            <img
                                src="images/buy1logo.svg"
                                width="150"
                                height="50"
                                className="d-inline-block align-top header-logo"
                                alt=""
                            />
                        </a>
                    </div>
                    <div className="block">
                        <a className="nav-link d-flex align-middle" href="#">
                            Dashboard
                            {/* <span className="sr-only">(current)</span> */}
                        </a>
                    </div>
                    <div className="block">
                        <a className="nav-link" href="#">
                            Products
                        </a>
                    </div>
                    <div className="dropdown block">
                        <a
                            className="nav-link dropdown-toggle"
                            href="#"
                            id="navbarDropdownMenuLink"
                            role="button"
                            data-toggle="dropdown"
                            aria-haspopup="true"
                            aria-expanded="false"
                        >
                            Vendor
                        </a>
                        <div
                            className="dropdown-menu"
                            aria-labelledby="navbarDropdownMenuLink"
                        >
                            <a className="dropdown-item" href="#">
                                Vendor
                            </a>
                            <a className="dropdown-item" href="#">
                                Payment
                            </a>
                            <a className="dropdown-item" href="#">
                                Orders
                            </a>
                        </div>
                    </div>
                    <div className="block">
                        <a className="nav-link" href="#">
                            Seller
                        </a>
                    </div>
                    <div className="block">
                        <a className="nav-link" href="#">
                            Orders
                        </a>
                    </div>
                    <div className="block">
                        <a className="nav-link" href="#">
                            Payment
                        </a>
                    </div>
                    <div className="block">
                        <a className="nav-link" href="#">
                            Analytics
                        </a>
                    </div>
                    <div className="block">
                        <a className="nav-link" href="#">
                            CMS
                        </a>
                    </div>
                    <div className="block">
                        <a className="nav-link" href="#">
                            Settings
                        </a>
                    </div>
                    <div className="block">
                        <a className="nav-link" href="#">
                            <img src="images/ic_search.svg" alt="" />
                        </a>
                    </div>
                    <div className="pt-2">
                        <div className="text-center">
                            <img
                                src="https://www.billboard.com/files/styles/article_main_image/public/media/Logic-red-carpet-vma-2017-13-billboard-1548.jpg"
                                alt=""
                                height="40"
                                width="40"
                                className="rounded-circle"
                            />
                        </div>
                        <div>
                            <HyperLink
                                size="12px"
                                transform="uppercase"
                                weight="bold"
                            >
                                Welcome, Alex
                            </HyperLink>
                        </div>
                    </div>
                </div>
            </div>
        );
    }
}
export default MasterHeader;
