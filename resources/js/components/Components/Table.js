import React from "react";

const Table = props => {
    return (
        <div>
            <table class="table table-hover">
                <thead>
                    <tr>
                        <th>
                            <input type="checkbox" />
                        </th>
                        {props.headers.map((header, index) => {
                            return <th scope={header.size}>{header.title}</th>;
                        })}
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <th>
                            <input type="checkbox" />
                        </th>
                        <th scope="row">1</th>
                        <td>Mark</td>
                        <td>Otto</td>
                        <td>@mdo</td>
                    </tr>
                    <tr>
                        <th scope="row">2</th>
                        <td>Jacob</td>
                        <td>Thornton</td>
                        <td>@fat</td>
                    </tr>
                    <tr>
                        <th scope="row">3</th>
                        <td colspan="2">Larry the Bird</td>
                        <td>@twitter</td>
                    </tr>
                </tbody>
            </table>
        </div>
    );
};
export default Table;
