import React from "react";

const BreadCrumb = props => {
    return (
        <nav aria-label="breadcrumb">
            <ol className="breadcrumb no-color">
                <li className="breadcrumb-item">
                    <a href="#">Home</a>
                </li>
                {props.items.map((item, index, array) => {
                    return (
                        <li
                            className="breadcrumb-item active text-success font-weight-bold"
                            aria-current="page"
                            key={index}
                        >
                            {item}
                        </li>
                    );
                })}
            </ol>
        </nav>
    );
};

export default BreadCrumb;
