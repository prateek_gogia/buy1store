// import "../node_modules/slick-carousel/slick/slick.css";
// import "../node_modules/slick-carousel/slick/slick-theme.css";

import "../../../../node_modules/slick-carousel/slick/slick.css";
import "../../../../node_modules/slick-carousel/slick/slick-theme.css";

import React, { Component } from "react";
import Slider from "react-slick";

export default class Carousel extends Component {
    render() {
        const settings = {
            dots: true,
            infinite: true,
            speed: 500,
            slidesToShow: 1,
            slidesToScroll: 1,
            adaptiveHeight: true
        };
        return (
            <div>
                <Slider {...settings}>
                    <div>
                        <img
                            src="images/login_image.jpg"
                            alt=""
                            className="img-carousel"
                        />
                    </div>
                    <div>
                        <img
                            src="images/login_image.jpg"
                            alt=""
                            className="img-carousel"
                        />
                    </div>
                </Slider>
            </div>
        );
    }
}
