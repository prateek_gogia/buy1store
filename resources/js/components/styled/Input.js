import styled from "styled-components";

const Input = styled.input`
    height: ${props => props.height};
    width: ${props => props.width};
    font-weight: ${props => props.weight};
    font-size: ${props => props.size};
`;

export default Input;
