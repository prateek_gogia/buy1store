import styled from "styled-components";

const Span = styled.span`
    font-weight: ${props => props.weight};
    font-size: ${props => props.size};
    text-transform: ${props => props.textStyle};
    margin-top: ${props => props.marginTop};
`;

export default Span;
