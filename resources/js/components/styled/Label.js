import styled from "styled-components";

const Label = styled.label`
    font-weight: ${props => props.weight};
    font-size: ${props => props.size};
`;

export default Label;
