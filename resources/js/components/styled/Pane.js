import styled from "styled-components";

const Pane = styled.div`
    padding-right: ${props => props.paddingRight};
    padding-left: ${props => props.paddingLeft};
`;

export default Pane;
