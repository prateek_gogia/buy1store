import styled from "styled-components";

const HyperLink = styled.a`
    font-weight: ${props => props.weight};
    font-size: ${props => props.size};
    text-transform: ${props => props.transform};
`;

export default HyperLink;
