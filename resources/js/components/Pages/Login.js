import React, { Component } from "react";
import Label from "../../styled/Label";
import Paragraph from "../../styled/Paragraph";
import Carousel from "../../styled/Carousel";
import HyperLink from "../../styled/HyperLink";
import Pane from "../../styled/Pane";

class Login extends Component {
    render() {
        return (
            <div className="container-fluid">
                <div className="row">
                    <div className="col-6">
                        <Carousel />
                    </div>
                    <Pane
                        className="col-6"
                        paddingRight="134px"
                        paddingLeft="136px"
                    >
                        <div>
                            <div className="text-center">
                                <img
                                    src="images/buy1logo.svg"
                                    alt=""
                                    height="50"
                                    width="150"
                                    className="big-logo"
                                />
                            </div>
                            <hr />
                            <Paragraph
                                size="20px"
                                weight="600"
                                textStyle="uppercase"
                            >
                                Sign In
                            </Paragraph>
                            <Paragraph className="text-muted">
                                Buy and sell businesses with here by Buy1.Promo
                            </Paragraph>
                            <Formik
                                initialValues={{ email: "", password: "" }}
                                validate={values => {
                                    let errors = {};
                                    if (!values.email) {
                                        errors.email = "Required";
                                    } else if (
                                        !/^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,}$/i.test(
                                            values.email
                                        )
                                    ) {
                                        errors.email = "Invalid email address";
                                    }
                                    return errors;
                                }}
                                onSubmit={(values, { setSubmitting }) => {
                                    setTimeout(() => {
                                        alert(JSON.stringify(values, null, 2));
                                        setSubmitting(false);
                                    }, 400);
                                }}
                            >
                                {({
                                    values,
                                    errors,
                                    touched,
                                    handleChange,
                                    handleBlur,
                                    handleSubmit,
                                    isSubmitting
                                    /* and other goodies */
                                }) => (
                                    <form onSubmit={handleSubmit}>
                                        <input
                                            type="email"
                                            name="email"
                                            onChange={handleChange}
                                            onBlur={handleBlur}
                                            value={values.email}
                                        />
                                        {errors.email &&
                                            touched.email &&
                                            errors.email}
                                        <input
                                            type="password"
                                            name="password"
                                            onChange={handleChange}
                                            onBlur={handleBlur}
                                            value={values.password}
                                        />
                                        {errors.password &&
                                            touched.password &&
                                            errors.password}
                                        <button
                                            type="submit"
                                            disabled={isSubmitting}
                                        >
                                            Submit
                                        </button>
                                    </form>
                                )}
                            </Formik>
                            <form action="" className="pt-2">
                                <div className="form-group mt-3">
                                    <Label weight="500" size="16px">
                                        E-mail
                                    </Label>
                                    <input
                                        type="email"
                                        className="form-control"
                                        placeholder="Email"
                                    />
                                </div>
                                <div className="form-group pt-2">
                                    <Label weight="500" size="16px">
                                        Password
                                    </Label>
                                    <HyperLink
                                        weight="400"
                                        size="14px"
                                        className="float-right"
                                        href="/forgot-password"
                                    >
                                        Forgot Password?
                                    </HyperLink>
                                    <input
                                        name="password"
                                        type="password"
                                        className="form-control"
                                        placeholder="Password"
                                    />
                                </div>
                                <input
                                    type="submit"
                                    className="btn btn-success col-12"
                                    value="Login"
                                />
                            </form>
                            <Paragraph className="text-center mt-4">
                                Don't have an account yet?{" "}
                                <HyperLink
                                    className="text-success"
                                    weight="bold"
                                    href="/"
                                >
                                    Sign Up
                                </HyperLink>
                            </Paragraph>
                        </div>
                    </Pane>
                </div>
            </div>
        );
    }
}
export default Login;
