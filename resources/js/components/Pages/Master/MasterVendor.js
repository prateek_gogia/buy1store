import React, { Component } from "react";
import MasterHeader from "../../Components/MasterHeader";
import BreadCrumb from "../../Components/BreadCrumb";
import Paragraph from "../../styled/Paragraph";
import Span from "../../styled/Span";
import Input from "../../styled/Input";
import Table from "../../Components/Table";

export default class MasterVendor extends Component {
    render() {
        return (
            <div>
                <MasterHeader />
                <BreadCrumb items={["Vendor"]} />
                <div className="container-fluid">
                    <Paragraph size="24px" weight="600">
                        Vendor
                    </Paragraph>
                    <div className="row">
                        <div className="col-3">
                            <div className="d-flex align-items-center justify-content-start">
                                <Span size="14px" className="pr-2">
                                    Vendor Name
                                </Span>
                                <Input
                                    type="text"
                                    height="40px"
                                    width="170px"
                                    className="form-control"
                                />
                            </div>
                        </div>
                        <div className="col-2">
                            <div className="d-flex justify-content-start align-items-center">
                                <Span size="14px" className="pr-2">
                                    Status
                                </Span>
                                <select className="form-control">
                                    <option value="All">All</option>
                                    <option value="Enabled">Enabled</option>
                                    <option value="Disabled">Disabled</option>
                                </select>
                            </div>
                        </div>
                        <div className="col-3" />
                        <div className="col-3">
                            <Input
                                type="text"
                                placeholder="Search by keywords"
                                width="100%"
                                className="form-control"
                            />
                        </div>
                        <div className="col-1">
                            <button className="btn btn-primary">Filter</button>
                        </div>
                    </div>
                </div>
                <div className="container-fluid mt-4">
                    <ul class="nav nav-tabs" id="myTab" role="tablist">
                        <li class="nav-item">
                            <a
                                class="nav-link active"
                                id="home-tab"
                                data-toggle="tab"
                                href="#home"
                                role="tab"
                                aria-controls="home"
                                aria-selected="true"
                            >
                                Vendor
                            </a>
                        </li>
                        <li class="nav-item">
                            <a
                                class="nav-link"
                                id="profile-tab"
                                data-toggle="tab"
                                href="#profile"
                                role="tab"
                                aria-controls="profile"
                                aria-selected="false"
                            >
                                Payment
                            </a>
                        </li>
                        <li class="nav-item">
                            <a
                                class="nav-link"
                                id="contact-tab"
                                data-toggle="tab"
                                href="#contact"
                                role="tab"
                                aria-controls="contact"
                                aria-selected="false"
                            >
                                Orders
                            </a>
                        </li>
                    </ul>
                    <div class="tab-content" id="myTabContent">
                        <div
                            class="tab-pane fade show active"
                            id="home"
                            role="tabpanel"
                            aria-labelledby="home-tab"
                        >
                            <Table
                                headers={[
                                    { title: "Vendor Name", size: 1 },
                                    {
                                        title:
                                            "No of Products/No of Products Active",
                                        size: 1
                                    },
                                    { title: "Sales to Date", size: 1 },
                                    { title: "Sales to Month", size: 1 },
                                    { title: "Open Orders", size: 1 },
                                    { title: "#RMA", size: 1 },
                                    { title: "Status", size: 1 },
                                    { title: " ", size: 4 }
                                ]}
                            />
                        </div>
                        <div
                            class="tab-pane fade"
                            id="profile"
                            role="tabpanel"
                            aria-labelledby="profile-tab"
                        >
                            ...
                        </div>
                        <div
                            class="tab-pane fade"
                            id="contact"
                            role="tabpanel"
                            aria-labelledby="contact-tab"
                        >
                            ...
                        </div>
                    </div>
                </div>
            </div>
        );
    }
}
