import React, { Component } from "react";
import Label from "../../styled/Label";
import Paragraph from "../../styled/Paragraph";
import Carousel from "../../styled/Carousel";
import HyperLink from "../../styled/HyperLink";
import Pane from "../../styled/Pane";

class ForgotPassword extends Component {
    render() {
        return (
            <div className="container-fluid">
                <div className="row">
                    <div className="col-6">
                        <Carousel />
                    </div>
                    <Pane
                        className="col-6"
                        paddingRight="134px"
                        paddingLeft="136px"
                    >
                        <div>
                            <div className="text-center">
                                <img
                                    src="images/buy1logo.svg"
                                    alt=""
                                    height="50"
                                    width="150"
                                    className="big-logo"
                                />
                            </div>
                            <hr />
                            <Paragraph
                                size="20px"
                                weight="700"
                                textStyle="uppercase"
                            >
                                Recover Password
                            </Paragraph>
                            <Paragraph className="text-muted">
                                Enter your Email and instructions will be sent
                                to you
                            </Paragraph>
                            <form action="" className="pt-2">
                                <div className="form-group mt-3">
                                    <Label weight="500" size="16px">
                                        E-mail
                                    </Label>
                                    <input
                                        type="email"
                                        className="form-control"
                                        placeholder="Email"
                                    />
                                </div>
                                <input
                                    type="submit"
                                    className="btn btn-success col-12 mt-4"
                                    value="Request Reset Link"
                                />
                            </form>
                            <div className="text-center mt-4">
                                <HyperLink href="/">Back to Login</HyperLink>
                            </div>
                        </div>
                    </Pane>
                </div>
            </div>
        );
    }
}
export default ForgotPassword;
