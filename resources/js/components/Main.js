import React, { Component } from "react";
import ReactDOM from "react-dom";
import { BrowserRouter, Route, Switch } from "react-router-dom";
import Login from "./Pages/Master/Login";
import ForgotPassword from "./Pages/Master/ForgotPassword";
import MasterVendor from "./Pages/Master/MasterVendor";
class App extends Component {
    render() {
        return (
            <BrowserRouter>
                <Switch>
                    <Route path="/forgot-password" component={ForgotPassword} />
                    <Route path="/master-vendor" component={MasterVendor} />
                    <Route path="/" component={Login} />
                </Switch>
            </BrowserRouter>
        );
    }
}

ReactDOM.render(<App />, document.getElementById("app"));
